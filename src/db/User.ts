import { model, Model, models, Schema } from 'mongoose';
import comparePassword from '_functions/user/comparePassword';
import generateAuthToken from '_functions/user/generateAuthToken';
import preFindOneAndUpdateUser from '_functions/user/preFindOneAndUpdateUser';
import preSaveUser from '_functions/user/preSaveUser';
import { UserModel } from '_types/models/UserModel/UserModel';

const schema = new Schema(
  {
    email: {
      type: String,
      required: true,
      unique: true,
    },
    password: {
      type: String,
      required: false,
    },
    firstName: {
      type: String,
      required: true,
    },
    lastName: {
      type: String,
      required: true,
    },
  },
  { timestamps: true, versionKey: false },
);

schema.pre('save', preSaveUser);
schema.pre('findOneAndUpdate', preFindOneAndUpdateUser);

schema.methods.comparePassword = comparePassword;
schema.methods.generateAuthToken = generateAuthToken;

const User = (models.User as Model<UserModel>) || (model('User', schema) as Model<UserModel>);

export default User;
