import { Document, model, models, PaginateModel, Schema } from 'mongoose';
import mongoosePaginate from 'mongoose-paginate-v2';
import { ActivityModel } from '_types/models/ActivityModel/ActivityModel.d';

const schema = new Schema(
  {
    userId: {
      type: Schema.Types.ObjectId,
      ref: 'User',
      required: true,
    },
  },
  { timestamps: true, versionKey: false, strict: false },
);

schema.plugin(mongoosePaginate);

const Activity =
  (models.Activity as PaginateModel<ActivityModel & Document>) ||
  (model('Activity', schema) as PaginateModel<ActivityModel & Document>);

export default Activity;
