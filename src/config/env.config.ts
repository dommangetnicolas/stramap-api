import dotenv from 'dotenv';
import TrustEnv from 'trust-env';

dotenv.config();

interface Env {
  MONGO_URI: string;
  JWT_KEY: string;
}

const env: Env = TrustEnv([
  {
    key: 'MONGO_URI',
    type: 'string',
    required: true,
  },
  {
    key: 'JWT_KEY',
    type: 'string',
    required: true,
  },
]);

export default env;
