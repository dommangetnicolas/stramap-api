import mongoose from 'mongoose';
import mongoosePaginate from 'mongoose-paginate-v2';
import logger from '_utils/logger';
import env from './env.config';

mongoosePaginate.paginate.options = {
  customLabels: {
    pagingCounter: false,
    hasPrevPage: false,
    hasNextPage: false,
    prevPage: false,
    nextPage: false,
  } as any,
};

mongoose.set('useNewUrlParser', true);
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);
mongoose.set('useUnifiedTopology', true);

const initDb = async (): Promise<void> => {
  try {
    await mongoose.connect(
      env.MONGO_URI,
      {
        useNewUrlParser: true,
        useCreateIndex: true,
        useUnifiedTopology: true,
      },
    );

    logger.info('Connected to MongoDB 🗄');
  } catch (err) {
    logger.error(`Can't connect to db: ${err}`);
    process.exit(1);
  }
};
export default initDb;
