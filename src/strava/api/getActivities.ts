import { AxiosResponse } from "axios";
import { http } from "../config";
import { Activity } from "../types";

const getActivities = (authToken: string): Promise<AxiosResponse<Activity[]>> =>
  http.get("/athlete/activities", {
    headers: {
      Authorization: `Bearer ${authToken}`,
    },
  });

export default getActivities;
