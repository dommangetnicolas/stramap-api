import { NextFunction, Request, Response } from 'express';
import passport from 'passport';
import passportJWT, { ExtractJwt, VerifiedCallback } from 'passport-jwt';
import env from '_config/env.config';
import { UserModel } from '_types/models/UserModel/UserModel';
import { ErrorHandler } from './errorMiddleware';

const JwtStrategy = passportJWT.Strategy;

const jwtOptions = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: env.JWT_KEY,
};

const jwtStrategy = new JwtStrategy(jwtOptions, (jwtPayload: UserModel, next: VerifiedCallback) => {
  const user = {
    _id: jwtPayload._id,
  };

  if (user && jwtPayload._id) {
    next(null, user);
    return;
  }

  next(null, false);
});

passport.use(jwtStrategy);

/* eslint-disable @typescript-eslint/no-unused-vars */
export const isAuthenticated = (req: Request, res: Response, next: NextFunction): void => {
  passport.authenticate('jwt', { session: false }, (err, user, info) => {
    if (err) {
      return next(err);
    }

    if (!user) {
      return next(new ErrorHandler(401, null, 'Unauthorized'));
    }

    req.user = user;
    return next();
  })(req, res, next);
};
/* eslint-enable @typescript-eslint/no-unused-vars */

export default passport;
