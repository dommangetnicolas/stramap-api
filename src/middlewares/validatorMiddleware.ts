import { NextFunction, Request, Response } from 'express';
import { ValidationChain, validationResult } from 'express-validator';
import { Types } from 'mongoose';
import { ErrorHandler } from './errorMiddleware';

export const validateObjectId = (value: string) => {
  return Types.ObjectId.isValid(value);
};

const validatorMiddleware = (validations: ValidationChain[]) => async (
  req: Request,
  res: Response,
  next: NextFunction,
): Promise<void> => {
  await Promise.all(validations.map((validation) => validation.run(req)));

  const errors = validationResult(req);
  if (errors.isEmpty()) {
    return next();
  }

  return next(
    new ErrorHandler(422, null, {
      message: 'Invalid parameters',
      data: errors,
    }),
  );
};

export default validatorMiddleware;
