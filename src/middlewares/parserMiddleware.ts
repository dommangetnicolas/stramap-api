import { NextFunction, Request, Response } from 'express';

const parserMiddleware = (req: Request, res: Response, next: NextFunction): void => {
  Object.keys(req.query).forEach((key) => {
    const value = req.query[key];

    if (typeof value === 'string') {
      try {
        req.query[key] = JSON.parse(value);
      } catch (_) {
        // Let the process continue
      }
    }
  });

  next();
};

export default parserMiddleware;
