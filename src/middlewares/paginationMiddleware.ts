import { Request, Response, NextFunction } from 'express';

const paginationMiddleware = (req: Request, res: Response, next: NextFunction): void => {
  const pagination = {
    page: req?.body?.page?.index,
    limit: req?.body?.page?.size,
    sort: req?.body?.sort,
  };

  Object.keys(pagination).forEach((key) =>
    (pagination as any)[key] === undefined ? delete (pagination as any)[key] : {},
  );

  req.body.pagination = pagination;

  next();
};

export default paginationMiddleware;
