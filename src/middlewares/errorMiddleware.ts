import { NextFunction, Request, Response } from 'express';
import { ErrorCode } from '_types/enums/ErrorCode';
import { IErrorResponse } from '_types/IErrors';
import logger from '_utils/logger';

interface IError {
  statusCode: number;
  controllerName: string;
}

export class ErrorHandler implements IErrorResponse {
  statusCode: number;

  controllerName: string;

  message: string;

  errorCode?: ErrorCode;

  data?: unknown;

  constructor(
    statusCode: number,
    controllerName: string,
    data: string | Omit<IErrorResponse, 'statusCode' | 'controllerName'>,
  ) {
    this.statusCode = statusCode;
    this.controllerName = controllerName;

    if (typeof data === 'string') {
      this.message = data;
    }

    if (typeof data === 'object') {
      this.errorCode = data.errorCode;
      this.message = data.message;
      this.data = data.data;
    }
  }
}

export const logError = (response: IErrorResponse, routeName: string): void => {
  logger.httpError(response, routeName);
};

/* eslint-disable @typescript-eslint/no-unused-vars */
const errorMiddleware = (
  err: Error & IError,
  req: Request,
  res: Response,
  next: NextFunction,
): Response => {
  const { statusCode: statusCodeFromErr, message, controllerName } = err;

  const statusCode = statusCodeFromErr || 500;

  const response = {
    status: 'error',
    statusCode,
    message,
    controllerName,
  };

  logError(response, req.originalUrl);

  delete response.controllerName;

  return res.status(statusCode).json(response);
};
/* eslint-enable @typescript-eslint/no-unused-vars */

export default errorMiddleware;
