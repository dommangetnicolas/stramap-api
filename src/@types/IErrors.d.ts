import { ErrorCode } from './enums/ErrorCode.d';

export interface IErrorResponse {
  statusCode: number;

  controllerName: string;

  message: string;

  errorCode?: ErrorCode;

  data?: unknown;
}
