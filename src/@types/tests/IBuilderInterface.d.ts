export interface IBuilderInterface<T> {
  getData: () => T;
  migrate(): Promise<any>;
}
