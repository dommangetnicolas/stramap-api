declare namespace Express {
  import { Schema } from 'mongoose';

  export interface User {
    _id: Schema.Types.ObjectId;
  }
}
