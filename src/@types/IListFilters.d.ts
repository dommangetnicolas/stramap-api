export interface IListFilters {
  pagination?: {
    sort?: Object | string;
    page: number;
    limit: number;
  };
}
