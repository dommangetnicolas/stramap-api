import { Document } from 'mongoose';
import comparePassword from '_functions/user/comparePassword';
import generateAuthToken from '_functions/user/generateAuthToken';

export type UserModel = Document & {
  email: string;

  password: string;

  firstName: string;

  lastName: string;

  createdAt: Date;

  updatedAt: Date;

  comparePassword?: typeof comparePassword;

  generateAuthToken?: typeof generateAuthToken;
};
