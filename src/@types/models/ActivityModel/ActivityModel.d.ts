import { Document } from 'mongoose';
import { Activity } from '_strava/types';

export type ActivityModel = Omit<Activity, 'id'> & {
  userId: Schema.Types.ObjectId;

  stravaId: number;
};
