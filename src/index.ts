import cors from 'cors';
import express from 'express';
import 'module-alias/register';
import path from 'path';
import initDb from '_config/db.config';
import passport from '_middlewares/authMiddleware';
import errorMiddleware from '_middlewares/errorMiddleware';
import paginationMiddleware from '_middlewares/paginationMiddleware';
import parserMiddleware from '_middlewares/parserMiddleware';
import router from '_routes/index';

if (process.env.NODE_ENV !== 'test') {
  initDb();
}

const app = express();

app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, '/views'));

app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(cors());

app.use(passport.initialize());

app.use(paginationMiddleware);
app.use(parserMiddleware);

app.use('/public', express.static('public'));
app.use('/', router());

app.use(errorMiddleware);

if (process.env.NODE_ENV !== 'test') {
  app.listen(3000);
}

export default app;
