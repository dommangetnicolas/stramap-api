import { body } from 'express-validator';

const validators = {
  import: [body('stravaToken').isString()],
};

export default validators;
