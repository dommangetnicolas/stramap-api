import { Request, Response, Router } from 'express';
import ActivityController from '_controllers/ActivityController';
import { isAuthenticated } from '_middlewares/authMiddleware';
import validatorMiddleware from '_middlewares/validatorMiddleware';
import validators from './validators';

const activityRoutes = (): Router => {
  const router = Router();

  router.post(
    '/list',
    isAuthenticated,
    (req: Request, res: Response, next) => {
      new ActivityController(req, res, next).list();
    },
  );

  router.post(
    '/import',
    isAuthenticated,
    validatorMiddleware(validators.import),
    (req: Request, res: Response, next) => {
      new ActivityController(req, res, next).import();
    },
  );

  return router;
};

export default activityRoutes;
