import { Router } from 'express';
import activityRoutes from './activityRoutes';
import authRoutes from './authRoutes';

const userRouter = (): Router => {
  const router = Router();

  router.use('/activity', activityRoutes());
  router.use('/auth', authRoutes());

  return router;
};

export default userRouter;
