import { Request, Response, Router } from 'express';
import AuthController from '_controllers/AuthController';

const authRoutes = (): Router => {
  const router = Router();

  router.post('/signin', (req: Request, res: Response, next) => {
    new AuthController(req, res, next).signin();
  });

  router.post('/signup', (req: Request, res: Response, next) => {
    new AuthController(req, res, next).signup();
  });

  return router;
};

export default authRoutes;
