import { Router } from 'express';
import userRouter from './user';

const defaultRouter = (): Router => {
  const router = Router();

  router.use('/v1', userRouter());

  return router;
};

export default defaultRouter;
