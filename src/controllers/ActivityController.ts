import BaseController from '_controllers/BaseController';
import Activity from '_db/Activity';
import upsertManyActivity from '_functions/activity/upsertManyActivity';
import { getActivities } from '_strava/index';
import { ActivityModel } from '_types/models/ActivityModel/ActivityModel';
import { ImportActivity } from '_types/models/ActivityModel/ImportActivity';
import { ListActivity } from '_types/models/ActivityModel/ListActivity';

class ActivityController extends BaseController {
  public list = (): Promise<void> =>
    this.execute('ActivityController.list', async () => {
      const { _id: userId } = this.req.user;
      const { pagination } = this.req.body as ListActivity;

      const list = await Activity.paginate({ userId }, pagination);

      return this.result(list);
    });

  public import = (): Promise<void> =>
    this.execute('ActivityController.import', async () => {
      const { _id: userId } = this.req.user;
      const { stravaToken } = this.req.body as ImportActivity;

      const { data } = await getActivities(stravaToken);

      const newActivities: ActivityModel[] = data.map((item) => ({
        ...item,
        stravaId: item.id,
        userId,
      }));

      await upsertManyActivity(newActivities);

      return this.created();
    });
}

export default ActivityController;
