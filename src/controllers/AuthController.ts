import BaseController from '_controllers/BaseController';
import User from '_db/User';
import emailAvailable from '_functions/auth/emailAvailable';
import { Signin } from '_types/models/AuthModel/Signin';
import { Signup } from '_types/models/AuthModel/Signup';

class AuthController extends BaseController {
  public signin = (): Promise<void> =>
    this.execute('AuthController.signin', async () => {
      const { email, password } = this.req.body as Signin;

      const user = await User.findOne({ email }, { updatedAt: 0 });

      if (!user || !user.comparePassword(password)) {
        return this.unauthorized('Invalid credentials');
      }

      const token = await user.generateAuthToken();

      const userInfos = { ...user.toJSON() };
      delete userInfos.password;

      return this.result({ user: userInfos, token });
    });

  public signup = (): Promise<void> =>
    this.execute('AuthController.signup', async () => {
      const { firstName, lastName, email, password } = this.req.body as Signup;

      if (!(await emailAvailable(email))) {
        return this.conflict('Email already in use');
      }

      const user = new User({
        firstName,
        lastName,
        email,
        password,
      });

      const userResult = { ...(await user.save()).toJSON() };
      delete userResult.password;

      const token = await user.generateAuthToken();

      return this.created({ user: userResult, token });
    });
}

export default AuthController;
