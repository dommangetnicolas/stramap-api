import { NextFunction, Request, Response } from 'express';
import { ErrorHandler } from '_middlewares/errorMiddleware';
import { IErrorResponse } from '_types/IErrors';
import logger from '_utils/logger';

class BaseController {
  protected req: Request;

  protected res: Response;

  protected next: NextFunction;

  protected name: string;

  constructor(req: Request, res: Response, next: NextFunction) {
    this.req = req;
    this.res = res;
    this.next = next;
  }

  protected async execute(controllerName: string, toExecuteFunction: () => void): Promise<void> {
    try {
      this.name = controllerName;
      this.logIncoming();
      await toExecuteFunction();
    } catch (err) {
      this.next(err);
    }
  }

  protected logIncoming() {
    const { query, body, params } = this.req;

    logger.log(this.name, '\n', {
      ...(!!Object.keys(query).length && { query }),
      ...(!!Object.keys(body).length && { body }),
      ...(!!Object.keys(params).length && { params }),
    });
  }

  protected result(payload?: unknown): void | Response {
    if (payload) {
      return this.res.status(200).json(payload);
    }

    return this.res.status(200).end();
  }

  protected updated(nbOfRow: number, payload?: unknown): void | Response {
    return this.res.status(200).json({ updated: nbOfRow, data: payload ?? {} });
  }

  protected created(payload?: unknown): void | Response {
    if (payload) {
      return this.res.status(201).json(payload);
    }

    return this.res.status(201).end();
  }

  protected render(status: number, fileName: string) {
    this.res.status(status).render(fileName);
  }

  protected deleted(): void {
    return this.res.status(204).end();
  }

  protected redirect(target: string): void {
    return this.res.redirect(301, target);
  }

  protected unauthorized(data?: string | Omit<IErrorResponse, 'statusCode'>): void {
    return this.next(
      new ErrorHandler(401, this.name, {
        message: 'Unauthorized',
        ...(typeof data === 'string' && { message: data }),
        ...(typeof data === 'object' && data),
      }),
    );
  }

  protected paymentRequired(data?: string | Omit<IErrorResponse, 'statusCode'>): void {
    return this.next(
      new ErrorHandler(402, this.name, {
        message: 'Unauthorized',
        ...(typeof data === 'string' && { message: data }),
        ...(typeof data === 'object' && data),
      }),
    );
  }

  protected forbidden(data?: string | Omit<IErrorResponse, 'statusCode'>): void {
    return this.next(
      new ErrorHandler(403, this.name, {
        message: 'Forbidden',
        ...(typeof data === 'string' && { message: data }),
        ...(typeof data === 'object' && data),
      }),
    );
  }

  protected notFound(data?: string | Omit<IErrorResponse, 'statusCode'>): void {
    return this.next(
      new ErrorHandler(404, this.name, {
        message: 'Not found',
        ...(typeof data === 'string' && { message: data }),
        ...(typeof data === 'object' && data),
      }),
    );
  }

  protected conflict(data?: string | Omit<IErrorResponse, 'statusCode'>): void {
    return this.next(
      new ErrorHandler(409, this.name, {
        message: 'Conflict',
        ...(typeof data === 'string' && { message: data }),
        ...(typeof data === 'object' && data),
      }),
    );
  }

  protected unprocessable(data?: string | Omit<IErrorResponse, 'statusCode'>): void {
    return this.next(
      new ErrorHandler(422, this.name, {
        message: 'Unprocessable',
        ...(typeof data === 'string' && { message: data }),
        ...(typeof data === 'object' && data),
      }),
    );
  }

  protected unhandled(err: Error): void {
    return this.next(new ErrorHandler(500, this.name, `Unhandled: ${err.message}`));
  }

  protected todo(): void {
    return this.next(new ErrorHandler(400, this.name, 'TODO'));
  }
}

export default BaseController;
