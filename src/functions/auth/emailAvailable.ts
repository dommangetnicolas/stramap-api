import User from '_db/User';

const emailAvailable = async (email: string): Promise<boolean> => {
  const user = await User.findOne({ email });

  return Boolean(!user);
};

export default emailAvailable;
