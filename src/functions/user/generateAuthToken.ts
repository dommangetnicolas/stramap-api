import { UserModel } from '_types/models/UserModel/UserModel';
import generateJwtToken from './generateJwtToken';

async function generateAuthToken(): Promise<string> {
  const user = this as UserModel;

  return generateJwtToken(user._id);
}

export default generateAuthToken;
