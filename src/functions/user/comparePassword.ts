import bcrypt from 'bcryptjs';

function comparePassword(candidatePassword: string): boolean {
  return bcrypt.compareSync(candidatePassword, this.password);
}

export default comparePassword;
