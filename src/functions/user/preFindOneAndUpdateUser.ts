import bcrypt from 'bcryptjs';
import { HookNextFunction } from 'mongoose';
import { UserModel } from '_types/models/UserModel/UserModel';

function preFindOneAndUpdateUser(next: HookNextFunction): void {
  const user = this.getUpdate() as UserModel;

  if (user.password) {
    user.password = bcrypt.hashSync(user.password, 10);
  }

  if (user.firstName) {
    user.firstName = user?.firstName?.toLocaleLowerCase?.().replace(/\s+/g, ' ').trim();
  }

  if (user.lastName) {
    user.lastName = user?.lastName?.toLocaleLowerCase?.().replace(/\s+/g, ' ').trim();
  }

  return next();
}

export default preFindOneAndUpdateUser;
