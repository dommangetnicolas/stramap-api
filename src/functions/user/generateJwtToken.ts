import jwt from 'jsonwebtoken';
import { Schema } from 'mongoose';
import env from '_config/env.config';

const generateJwtToken = (userId: string | Schema.Types.ObjectId) => {
  return jwt.sign({ _id: userId }, env.JWT_KEY, {
    expiresIn: '30days',
  });
};

export default generateJwtToken;
