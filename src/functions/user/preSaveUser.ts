import bcrypt from 'bcryptjs';
import { HookNextFunction } from 'mongoose';
import { UserModel } from '_types/models/UserModel/UserModel';

function preSaveUser(next: HookNextFunction): void {
  const user = this as UserModel;

  if (user.isModified('password')) {
    user.password = bcrypt.hashSync(user.password, 10);
  }

  if (user.isModified('firstName')) {
    user.firstName = user?.firstName?.toLocaleLowerCase?.().replace(/\s+/g, ' ').trim();
  }

  if (user.isModified('lastName')) {
    user.lastName = user?.lastName?.toLocaleLowerCase?.().replace(/\s+/g, ' ').trim();
  }

  return next();
}

export default preSaveUser;
