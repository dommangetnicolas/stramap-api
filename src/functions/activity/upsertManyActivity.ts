import Activity from '_db/Activity';
import { ActivityModel } from '_types/models/ActivityModel/ActivityModel';

const upsertManyActivity = async (activities: ActivityModel[]) => {
  const promises = activities.map(async (item) => {
    const isExisting = await Activity.findOne({ stravaId: item.stravaId });

    if (isExisting) return;

    await Activity.create(item);
  });

  await Promise.all(promises);
};

export default upsertManyActivity;
